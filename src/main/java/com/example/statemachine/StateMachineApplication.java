package com.example.statemachine;

import com.example.statemachine.domain.User;
import com.example.statemachine.dto.UserDto;
import com.example.statemachine.enums.UserStatus;
import com.example.statemachine.repository.UserRepository;
import com.example.statemachine.service.TransitionService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@RequiredArgsConstructor
@SpringBootApplication
public class StateMachineApplication implements CommandLineRunner {
    private final TransitionService<UserDto> transitionService;
    private final UserRepository userRepository;

    public static void main(String[] args) {
        SpringApplication.run(StateMachineApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        User user = User.builder()
                .firstName("Murad")
                .lastName("Gadirov")
                .status(UserStatus.DRAFT)
                .build();
        userRepository.save(user);
        transitionService.transition(user.getId(),"SUBMIT");

    }
}
