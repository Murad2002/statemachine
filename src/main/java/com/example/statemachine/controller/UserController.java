package com.example.statemachine.controller;

import com.example.statemachine.dto.UserProcessStartDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {

    @PostMapping("/start-process")
    public ResponseEntity<?> isPseudoPanBelongsIpsAcc(@RequestBody UserProcessStartDto dto) {
        return ResponseEntity.noContent().build();
    }
}
