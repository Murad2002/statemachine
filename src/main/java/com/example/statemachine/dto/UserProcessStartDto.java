package com.example.statemachine.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Pattern;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserProcessStartDto {
    private static final String NOT_BLANK_MESSAGE = "should not be blank or null";
    private static final String PATTERN_MESSAGE = "A phone number must satisfy given constraint";
    @NotBlank(message = NOT_BLANK_MESSAGE)
    @Pattern(regexp = "^(?=.*[@.])(.{30,})$\n", message = PATTERN_MESSAGE)
    String email;
    @NotBlank(message = NOT_BLANK_MESSAGE)
    @Pattern(regexp = "^[0-9+]{0,15}", message = PATTERN_MESSAGE)
    String phoneNumber;
}
