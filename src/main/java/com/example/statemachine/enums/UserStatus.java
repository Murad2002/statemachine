package com.example.statemachine.enums;


import java.util.Arrays;
import java.util.List;

import com.example.statemachine.user.transitions.Approve;
import com.example.statemachine.user.transitions.Notify;
import com.example.statemachine.user.transitions.Reject;
import com.example.statemachine.user.transitions.Submit;

public enum UserStatus {

    DRAFT(Submit.NAME),
    IN_REVIEW(Approve.NAME, Reject.NAME),
    APPROVED(Approve.NAME, Notify.NAME),
    NOTIFIED();


    private final List<String> transitions;

    UserStatus(String... transitions) {
        this.transitions = Arrays.asList(transitions);
    }

    public List<String> getAllowedTransitions() {
        return transitions;
    }

}