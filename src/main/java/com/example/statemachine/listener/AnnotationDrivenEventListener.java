package com.example.statemachine.listener;

import com.example.statemachine.service.impl.TransitionServiceImpl;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class AnnotationDrivenEventListener {
    @EventListener(classes = {TransitionServiceImpl.class})
    public void handleContextStart(TransitionServiceImpl o) {
        System.out.println("AAA " + o.getClass().getName());
    }
}
