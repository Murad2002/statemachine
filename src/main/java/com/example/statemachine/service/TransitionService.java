package com.example.statemachine.service;

import java.util.List;

public interface TransitionService<T> {

    T transition(Long id, String transition);

    List<String> getAllowedTransition(Long id);
}
