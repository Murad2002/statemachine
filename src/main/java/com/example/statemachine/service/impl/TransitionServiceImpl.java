package com.example.statemachine.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.example.statemachine.domain.User;
import com.example.statemachine.dto.UserDto;
import com.example.statemachine.enums.UserStatus;
import com.example.statemachine.repository.UserRepository;
import com.example.statemachine.service.TransitionService;
import com.example.statemachine.user.Transition;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TransitionServiceImpl implements TransitionService<UserDto> {

    private final UserRepository userRepository;
    private final ModelMapper mapper;
    private final ApplicationEventPublisher publisher;
    private final List<Transition<UserDto>> transitionList;
    private Map<String, Transition<UserDto>> transitionMap;

    @Override
    public UserDto transition(Long id, String transition) {
        Transition<UserDto> userDtoTransition = Optional.ofNullable(transitionMap.get(transition))
                .orElseThrow(RuntimeException::new);
        publisher.publishEvent(this);
        return userRepository.findById(id)
                .map(user -> {
                    checkAllowed(transition, user.getStatus());
                    userDtoTransition.applyProcessing(mapper.map(user, UserDto.class));
                    return updateUserStatus(user, userDtoTransition.getTargetStatus());
                })
                .map(user -> mapper.map(user, UserDto.class))
                .orElseThrow(RuntimeException::new);
    }


    @Override
    public List<String> getAllowedTransition(Long id) {
        return userRepository.findById(id).orElseThrow(RuntimeException::new)
                .getStatus()
                .getAllowedTransitions();
    }

    private void checkAllowed(String transition, UserStatus status) {
        boolean isAllowed = status.getAllowedTransitions().contains(transition);
        if (!isAllowed) {
            throw new RuntimeException();
        }
    }

    private User updateUserStatus(User user, UserStatus targetStatus) {
        user.setStatus(targetStatus);
        return userRepository.save(user);
    }

    @PostConstruct
    private void initTransitions() {
        Map<String, Transition<UserDto>> transitionHashMap = new HashMap<>();
        for (Transition<UserDto> transition : transitionList) {
            if (transitionHashMap.containsKey(transition.getName())) {
                throw new IllegalStateException("Duplicate transition !");
            }
            transitionHashMap.put(transition.getName(), transition);
        }
        this.transitionMap = transitionHashMap;
    }
}
