package com.example.statemachine.user;


import com.example.statemachine.enums.UserStatus;

public interface Transition<T> {


    String getName();

    UserStatus getTargetStatus();

    void applyProcessing(T order);


}