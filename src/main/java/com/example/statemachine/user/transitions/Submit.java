package com.example.statemachine.user.transitions;

import com.example.statemachine.dto.UserDto;
import com.example.statemachine.enums.UserStatus;
import com.example.statemachine.user.Transition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Submit implements Transition<UserDto> {

    public static final String NAME = "SUBMIT";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public UserStatus getTargetStatus() {
        return UserStatus.IN_REVIEW;
    }

    @Override
    public void applyProcessing(UserDto order) {
        log.info("User with id {} is transitioning to {} state", order.getId(), getTargetStatus());
        if (order.getUserName().equals("murad")) {
            throw new RuntimeException("Illegal");
        }
    }
}
